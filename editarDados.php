<?php
include_once 'includes/Pessoa.class.php';
session_start();

if(($_POST['password'] !== '' || $_POST['second_password'] !== '') && ($_POST['password'] == $_POST['second_password'])){
  editarDados($_POST['nome'], md5($_POST['password']), $_SESSION['currentUser']->getEmail());
}else{
  editarSemSenha($_POST['nome'],$_SESSION['currentUser']->getEmail());
}
$filename = $_SESSION['currentUser']->getEmail().'_picture';
if($_FILES['foto']['name'] != ''){
  $pictureName = uploadPicture($filename);
  $_SESSION['currentUser']->setFoto($pictureName);
}


function editarDados($nome, $senha, $email){
    $bdInstance = BD::getInstance();
    $sql = "UPDATE Pessoa SET nome=?, password=? WHERE email=?";
    $stmt = $bdInstance->prepare($sql);
    if($stmt == TRUE){
      $stmt->bind_param("sss", $nome, $senha, $email);
      $stmt->execute();
      $stmt->close();
      $_SESSION['currentUser']->setNome($nome);
      header('Location: ponto.php');
    }else{
      echo $bdInstance->error;
    }

}

function editarSemSenha($nome,$email){
  $bdInstance = BD::getInstance();
  $sql = "UPDATE Pessoa SET nome=? WHERE email=?";
  $stmt = $bdInstance->prepare($sql);
  if($stmt == TRUE){
    $stmt->bind_param("ss", $nome, $email);
    $stmt->execute();
    $stmt->close();
    $_SESSION['currentUser']->setNome($nome);
    // $_SESSION['currentUser']->setFoto($foto);
    header('Location: ponto.php');
  }else{
    echo $bdInstance->error;
  }
}

function uploadPicture($filename){
  if(!isset($_FILES['foto'])) return false;
  $allowedExts = array("gif", "jpeg", "jpg", "png");
  $allowedTypes = array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
  $temp = explode(".",$_FILES['foto']['name']);
  $extension = end($temp);
  if(in_array($extension,$allowedExts) && in_array($_FILES['foto']['type'],$allowedTypes)){
    move_uploaded_file($_FILES['foto']['tmp_name'],"uploads/$filename.$extension");
    return "$filename.$extension";
  }else{
    return false;
  }
}


?>
