-- phpMyAdmin SQL Dump
-- version 4.0.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2014 at 12:30 PM
-- Server version: 5.6.13
-- PHP Version: 5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pontoEletronico`
--

-- --------------------------------------------------------

--
-- Table structure for table `Pessoa`
--

CREATE TABLE IF NOT EXISTS `Pessoa` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `password` varchar(50) NOT NULL,
  `role` varchar(20) DEFAULT 'user',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

--
-- Dumping data for table `Pessoa`
--

INSERT INTO `Pessoa` (`id`, `nome`, `email`, `foto`, `password`, `role`) VALUES
(24, 'PauloIgual', 'paulo@email.com', NULL, '202cb962ac59075b964b07152d234b70', 'user'),
(26, 'Paulito certo', 'paulitolinhares@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'user'),
(27, 'paulofoto', 'paulitofoto@gmail.com', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'user'),
(36, 'Paulo Linhares Neto', 'plneto@email.com', 'plneto@email.compicturejpg', 'e10adc3949ba59abbe56e057f20f883e', 'user'),
(38, 'Paulo Linhares', 'pl@email.com', 'pl@email.compicture.jpg', 'e10adc3949ba59abbe56e057f20f883e', 'user'),
(39, 'Paulo com nome certo novamente', 'reg@email.com', 'reg@email.com_picture.png', 'c33367701511b4f6020ec61ded352059', 'user'),
(40, 'Paulo Linhares', 'paulo@example.com', 'paulo@example.com_picture.png', 'e10adc3949ba59abbe56e057f20f883e', 'user'),
(42, 'Paulo Linhares Neto', 'paulolinharesneto@email.com', 'paulolinharesneto@email.com_picture.jpg', 'e10adc3949ba59abbe56e057f20f883e', 'user');

-- --------------------------------------------------------

--
-- Table structure for table `Ponto`
--

CREATE TABLE IF NOT EXISTS `Ponto` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `horaInicial` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `horaFinal` timestamp NULL DEFAULT '0000-00-00 00:00:00',
  `id_pessoa` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

--
-- Dumping data for table `Ponto`
--

INSERT INTO `Ponto` (`id`, `horaInicial`, `horaFinal`, `id_pessoa`) VALUES
(61, '2014-04-01 01:50:53', '2014-04-01 01:50:53', 40),
(62, '2014-04-01 01:52:31', '2014-04-01 01:52:31', 40),
(63, '2014-04-01 11:08:12', '2014-04-01 11:08:12', 40);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
