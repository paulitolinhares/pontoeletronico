    <?php
    /**
    * Classe que trata a conexão do sistema com o banco de dados
    */
    class BD{
    private static $dbLink;

    public static function getInstance(){
    $dbinfo = self::getCredentials('includes/bd.xml');
    $dbLink = new mysqli($dbinfo['host'],$dbinfo['user'],$dbinfo['password'],$dbinfo['database']);
    return $dbLink;
    }


    private static function getCredentials($filename){
        if(!file_exists($filename)){
            exit("Erro ao tentar abrir $filename");

        }else{
            $xml = simplexml_load_file($filename);
            //var_dump($xml);
            $result = array();
            foreach ($xml as $key => $value) {
               $result[$key] = (String)$value;

                    }
                }
             return $result;
            }


        }


    ?>
