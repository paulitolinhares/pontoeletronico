<?php
include_once 'includes/Pessoa.class.php';
session_start();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Editar Dados</title>
  <link rel="stylesheet" href="css/gumby.css" type="text/css" media="screen" title="no title" charset="utf-8">
</head>
<body class="editar">
  <h1>Editar Dados</h1>
  <div class="row">
    <div class="six columns centered">
      <div class="form_container">
        <form action="editarDados.php" method="post" enctype="multipart/form-data">
          <div class="field">
            <label for="email">E-mail: </label>
            <input id="email" name="email" type="email" value="<?php echo $_SESSION['currentUser']->getEmail() ?>" class="input" disabled>
          </div>
          <div class="field">
            <label for="name">Nome completo: </label>
            <input id="name" name="nome" type="text" value="<?php echo $_SESSION['currentUser']->getNome() ?>" class="input">
          </div>
          <div class="field">
            <label for="password">Nova Senha: </label>
            <input id="password" name="password" type="password" class="input">
            <label for="second_password">Repetir Nova Senha: </label>
            <input id="second_password" name="second_password" type="password" class="input">
          </div>
          <div>
            <label style="display: inline;" for="foto">Foto: </label>
            <input type="file" name="foto" id="foto">
          </div>
          <input type="submit">
        </form>
      </div>
    </div>
  </div>
</body>
</html>
