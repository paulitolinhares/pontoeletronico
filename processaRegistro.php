<?php
  include_once 'includes/BD.class.php';
  include_once 'includes/Pessoa.class.php';

  //$email = $_POST['email'];

  function validaEmail($email){
	//verifica se e-mail esta no formato correto de escrita
	if (!preg_match('^([a-zA-Z0-9.-])*([@])([a-z0-9]).([a-z]{2,3})^',$email)){

		return false;
    }
		else{return true;} // Retorno true para indicar que o e-mail é valido

  }
  if(validaEmail($_POST['email'])){
    $filename = $_POST['email'].'_picture';
    echo $filename;
    $pictureName = uploadPicture($filename);
    if($pictureName !== FALSE){
      echo "pictureName: $pictureName<br>";
      $_POST['password'] = md5($_POST['password']);
      $pessoa = new Pessoa($_POST['nome'],$_POST['email'],$_POST['password'],$pictureName,'user');
      print_r($pessoa);
      $pessoa->registrar();
      session_start();
      $_SESSION['currentUser'] = $pessoa;
      $_SESSION['logado'] = true;
      header('Location: ponto.php');
    }
  }else{
  	$mensagem='E-mail Inv&aacute;lido!';
  	echo "$mensagem";
  }


function uploadPicture($filename){
  $allowedExts = array("gif", "jpeg", "jpg", "png");
  $allowedTypes = array("image/gif","image/jpeg","image/jpg","image/pjpeg","image/x-png","image/png");
  $temp = explode(".",$_FILES['foto']['name']);
  $extension = end($temp);
  if(in_array($extension,$allowedExts) && in_array($_FILES['foto']['type'],$allowedTypes)){
    move_uploaded_file($_FILES['foto']['tmp_name'],"uploads/$filename.$extension");
    return "$filename.$extension";
  }else{
    return false;
  }
}
?>
