<?php
  include_once 'includes/Pessoa.class.php';
  date_default_timezone_set('America/Recife');
  session_start();
  if($_SESSION['logado'] !== TRUE){
    header("Location: index.php?erro=Voce precisa esta logado para acessar esta pagina");
  }
  if($_SESSION['currentUser']){
    $nome = $_SESSION['currentUser']->getNome();
    $email = $_SESSION['currentUser']->getEmail();
  }else{
    #header('Location: index.php?erro=Voce precisa esta logado para acessar esta pagina');
  }

?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Ponto eletrônico</title>
  <link rel="stylesheet" href="css/gumby.css" type="text/css" media="screen" title="no title" charset="utf-8">
  <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.0.min.js" charset="utf-8"></script>
  <script type="text/javascript" charset="utf-8">
    $(function(){
      var acao = 'finalizar';
      $('#botao_ponto').click(function(){
        acao = acao == 'iniciar' ? 'finalizar' : 'iniciar';
        console.log('iniciando ponto');
        $.ajax({
          method: "POST",
          url: 'registraPonto.php',
          data: {
            acao: acao
          },
          success: function(data){
            $('.botao_ponto_container').toggleClass('started');
            var text = acao == 'finalizar' ? 'Iniciar Jornada' : 'Finalizar Jornada';
            console.log(text);
            $('#botao_ponto').html(text);
            $('#inicio_ponto').html(data);
          }
        });
      });
    });
  </script>
  <script type="text/javascript" charset="utf-8">
    function startTime(){
      var today=new Date();
      var h=today.getHours();
      var m=today.getMinutes();
      var s=today.getSeconds();
      // add a zero in front of numbers<10
      m=checkTime(m);
      s=checkTime(s);
      document.getElementById('relogio').innerHTML=h+":"+m+":"+s;
      t=setTimeout(function(){startTime()},500);
    }
    function checkTime(i){
      if (i<10){
        i="0" + i;
        }
      return i;
    }

  </script>
</head>
<body class="ponto"  onload="startTime()">
  <div class="row full">
    <div class="three columns">
      <img src="uploads/<?php echo $_SESSION['currentUser']->getFoto()?>" alt="" />
      <p>
        <?php echo "Olá, $nome"; ?>
      </p>
      <p>
        <?php echo $email; ?>
      </p>
      <a href="editar">
        Editar Dados
      </a>
      <form action="logout.php">
        <input type="submit" value="Sair">
      </form>
    </div>
    <div class="nine columns">
      <div class=" twelve columns centered relogio">
        <p id="relogio">
        </p>
      </div>
      <div class="three columns centered">
        <div class="botao_ponto_container">
          <p id="botao_ponto">Iniciar Jornada</p>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
