<?php
  include_once 'includes/BD.class.php';
  include_once 'includes/Pessoa.class.php';
  include_once 'includes/Ponto.class.php';
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Sistema de ponto eletrônico</title>
  <link rel="stylesheet" href="css/gumby.css" type="text/css" media="screen" title="no title" charset="utf-8">
</head>
<body class="index">
  <?php
      if(isset ($_GET['erro'])){
        echo "<p>Você precisa está logado para acessar esta página!</p>";
      }
  ?>
  <h1>Ponto eletrônico</h1>
  <div class="row">
    <div class="four columns centered">
      <div class="form_container">
        <form action="login.php" method="post">
          <div class="field">
            <label for="email">Email: </label>
            <input type="email" name="email" id="email" class="input">
          </div>
          <div class="field">
            <label for="senha">Senha: </label>
            <input type="password" name="password" class="input">
          </div>
          <input type="submit" value="Entrar">
        </form>
        <div class="medium primary btn"><a href="register">Criar conta</a></div>
      </div>
    </div>
  </div>
</body>
</html>
