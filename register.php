<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Registro</title>
  <link rel="stylesheet" href="css/gumby.css" type="text/css" media="screen" title="no title" charset="utf-8">
</head>
<body class="register">
  <h1>Criar conta</h1>
  <div class="row">
    <div class="six columns centered">
      <div class="form_container">
        <form action="processaRegistro.php" method="post" enctype="multipart/form-data">
          <div class="field">
            <label for="name">Nome completo: </label>
            <input id="name" name="nome" type="text" class="input">
          </div>
          <div class="field">
            <label for="email">E-mail: </label>
            <input id="email" name="email" type="text" class="input">
          </div>
          <div class="field">
            <label for="password">Senha: </label>
            <input id="password" name="password" type="password" class="input">
          </div>
          <label for="foto">Foto: </label>
          <input id="foto" type="file" name="foto">
          <br>
          <input type="submit">
        </form>
      </div>
    </div>
  </div>

</body>
</html>
